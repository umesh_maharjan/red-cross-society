package com.redcrosssociteyapp;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.redcrosssociteyapp.DatabaseManager.DatabaseManager;

/**
 * Created by govinda on 10/3/2016.
 */
public class SplashActivity extends Activity
{
    Handler handler;
    DatabaseManager mDatabaseManager;
    int checkDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);
        mDatabaseManager = new DatabaseManager(this);
        checkDb = mDatabaseManager.checkDatabase();

        run();
    }

    public void run() {
        handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {

                if(checkDb!=0){

                    if(!getSharedPreferences("Login Credential", Context.MODE_PRIVATE).getString("USERNAME","").equals("")){
                        startActivity(new Intent(SplashActivity.this,DashboardActivity.class));
                        finish();
                    }else{
                        startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                        finish();}
                }else{

                    startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                    finish();
                    // }
                }

            }
        };

        handler.postDelayed(r, 2000);
    }

}
