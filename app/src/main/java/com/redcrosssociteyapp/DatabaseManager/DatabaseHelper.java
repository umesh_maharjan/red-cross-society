package com.redcrosssociteyapp.DatabaseManager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper {
    public static String DATABASE_NAME = "db_redcross";// database name
    public static String USERID = "id"; // database id
    public static String DISTRICT_ID = "DistrictID"; // database id
    public static String DISTRICT_NAME = "DistrictName";
    public static String VDC_NAME = "VDCName";
    public static String VDC_ID = "VDCId";// database id

    public static String DISTRICT_INFO = "tbl_district";
    public static String VDC_INFO = "tbl_vdc";

    public static String SECTOR_INFO = "tbl_sector";
    public static String SECTOR_ID = "SectorId";
    public static String SECTOR_NAME = "SectorName";
    public static String ACTIVITY_TYPE_INFO = "tbl_ActivityType";// users table

    public static String PROGRAM_ID = "ProgramTypeId";

    public static String PROGRAM_NAME = "ProgramTypeName";

    public static String ACTIVITY_INFO = "tbl_Activity";

    public static String ACTIVITY_ID = "ActivityId";

    public static String ACTIVITY_NAME = "ActivityName";

    // users database table create
    String DistrictSqlTable = "CREATE TABLE IF NOT EXISTS " + DISTRICT_INFO + " ( "
            + USERID + " INTEGER PRIMARY KEY, " + DISTRICT_ID + " INTEGER, " + DISTRICT_NAME
            + " TEXT )";



    String VDCSqlTable = "CREATE TABLE IF NOT EXISTS " + VDC_INFO + " ( "
            + USERID + " INTEGER PRIMARY KEY, " + DISTRICT_ID + " INTEGER, "+ VDC_ID + " INTEGER, " + VDC_NAME
            + " TEXT )";


    String SectorSqlTable = "CREATE TABLE IF NOT EXISTS " + SECTOR_INFO + " ( "
            + USERID + " INTEGER PRIMARY KEY, " + SECTOR_ID + " INTEGER, "+ SECTOR_NAME
            + " TEXT )";


    String ActivitytypeSqlTable = "CREATE TABLE IF NOT EXISTS " + ACTIVITY_TYPE_INFO + " ( "
            + USERID + " INTEGER PRIMARY KEY, " + PROGRAM_ID + " INTEGER, "+ PROGRAM_NAME
            + " TEXT )";



    String ActivitySqlTable = "CREATE TABLE IF NOT EXISTS " + ACTIVITY_INFO + " ( "
            + USERID + " INTEGER PRIMARY KEY, " + ACTIVITY_ID + " INTEGER, "+ SECTOR_ID + " INTEGER, " + ACTIVITY_NAME
            + " TEXT )";

    // table for users....................////////////////////////////
    public static String USERNAME = "username";
    public static String PASSWORD = "password";
    public static String INFO_DATA = "info_data";
    public static String TABLE_USER_DATA = "tbl_user_data";//table name
    String createTableUsers = "CREATE TABLE IF NOT EXISTS " + TABLE_USER_DATA
            + " (id INTEGER PRIMARY KEY, "
            + USERNAME + " TEXT, "
            + PASSWORD + " TEXT, "
            + INFO_DATA + " TEXT)" ;
////.............................../////////////////////



    ///table for eventdata//////////////////////

    public static String JSON_DATA = "json_data";
    public static String DATE_TIME = "date_time";
    public static String FLAG = "flag";
    public static String DISTRICT = "district";
    public static String VDC = "vdc";
    public static String WARDNO = "wardno";
    public static String STARTDATE = "startdate";
    public static String TABLE_EVENT_FORM_DATA = "tbl_event_form_data";//table name
    String createTableEvent = "CREATE TABLE IF NOT EXISTS " + TABLE_EVENT_FORM_DATA
            + " (id INTEGER PRIMARY KEY, "
            + USERNAME + " TEXT, "
            + JSON_DATA + " TEXT, "
            + DISTRICT + " TEXT, "
            + VDC + " TEXT, "
            + WARDNO + " TEXT, "
            + STARTDATE + " TEXT, "
            + DATE_TIME + " NUMERIC,"
            + FLAG + " NUMERIC)" ;

    ///...................//////////////////////

    //table for monthly reports
    public static String TABLE_MONTHLY_REPORT = "tbl_monthly_report";//table name
    String createTableMonthly = "CREATE TABLE IF NOT EXISTS " + TABLE_MONTHLY_REPORT
            + " (id INTEGER PRIMARY KEY, "
            + USERNAME + " TEXT, "
            + JSON_DATA + " TEXT, "
            + DISTRICT + " TEXT, "
            + VDC + " TEXT, "
            + WARDNO + " TEXT, "
            + STARTDATE + " TEXT, "
            + DATE_TIME + " NUMERIC,"
            + FLAG + " NUMERIC)" ;

    ///...................//////////////////////


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(DistrictSqlTable);
        db.execSQL(VDCSqlTable);
        db.execSQL(SectorSqlTable);
        db.execSQL(ActivitytypeSqlTable);
        db.execSQL(ActivitySqlTable);
        db.execSQL(createTableUsers);//execute user table
        db.execSQL(createTableEvent);
        db.execSQL(createTableMonthly);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }
}
