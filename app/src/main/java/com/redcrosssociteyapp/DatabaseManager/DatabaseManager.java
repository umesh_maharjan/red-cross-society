package com.redcrosssociteyapp.DatabaseManager;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.redcrosssociteyapp.Model.AcitivityTypeInfo;
import com.redcrosssociteyapp.Model.ActivityNameInfo;
import com.redcrosssociteyapp.Model.DistrictInfo;
import com.redcrosssociteyapp.Model.JsonData_DTO;
import com.redcrosssociteyapp.Model.SectorInfo;
import com.redcrosssociteyapp.Model.VDCInfo;

import java.util.ArrayList;

public class DatabaseManager {



        SQLiteDatabase db;
        DatabaseManager databasemanager;
        DatabaseHelper helper;

        public DatabaseManager(Context context)

        {
            helper = new DatabaseHelper(context);

            db = helper.getWritableDatabase();

        }

    //related to user table////////////////////


    public void insertUserInfo(String username, String password,String userInfo) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.USERNAME, username);
        cv.put(DatabaseHelper.PASSWORD, password);
        cv.put(DatabaseHelper.INFO_DATA, userInfo);

        db.insert(DatabaseHelper.TABLE_USER_DATA, null, cv);


    }


    public String getUsername(String username) {

        String username1 = "";
        String sql1 = "Select username from tbl_user_data where username=\""+username+"\"";

        Cursor cursor = db.rawQuery(sql1, null);

        while (cursor.moveToNext()) {

            username1 = (cursor.getString(cursor
                    .getColumnIndex(DatabaseHelper.USERNAME)));

        }
        cursor.close();
        return username1;
    }

    public String getPassword(String password) {

        String password1 = "";
        String sql1 = "Select password from tbl_user_data where password=\""+password+"\"";

        Cursor cursor = db.rawQuery(sql1, null);

        while (cursor.moveToNext()) {

            password1 = (cursor.getString(cursor
                    .getColumnIndex(DatabaseHelper.PASSWORD)));

        }
        cursor.close();
        return password1;
    }

    public void deleteUser(String username) {

        db.delete(DatabaseHelper.TABLE_USER_DATA, "username=\"" + username+"\"", null);
    }

    public int checkDatabase(){
        String count = "SELECT count(*) FROM tbl_user_data";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        return icount;
    }


    //........................///////////////////



    //related to form data ////////////////


    public void insertJsondata(String username, String json_data, String date_time, String flag,String district,String vdc,String wardno,String startdate) {
        ContentValues cv = new ContentValues();

        cv.put(DatabaseHelper.USERNAME, username);
        cv.put(DatabaseHelper.JSON_DATA, json_data);
        cv.put(DatabaseHelper.DISTRICT, district);
        cv.put(DatabaseHelper.VDC, vdc);
        cv.put(DatabaseHelper.WARDNO, wardno);
        cv.put(DatabaseHelper.STARTDATE, startdate);
        cv.put(DatabaseHelper.DATE_TIME, date_time);
        cv.put(DatabaseHelper.FLAG, flag);
        db.insert(DatabaseHelper.TABLE_EVENT_FORM_DATA, null, cv);
        // TODO Auto-generated method stub

    }

//insert for monthly data
    public void insertMonthlyJsondata(String username, String json_data, String date_time, String flag,String district,String vdc,String wardno,String startdate) {
        ContentValues cv = new ContentValues();

        cv.put(DatabaseHelper.USERNAME, username);
        cv.put(DatabaseHelper.JSON_DATA, json_data);
        cv.put(DatabaseHelper.DISTRICT, district);
        cv.put(DatabaseHelper.VDC, vdc);
        cv.put(DatabaseHelper.WARDNO, wardno);
        cv.put(DatabaseHelper.STARTDATE, startdate);
        cv.put(DatabaseHelper.DATE_TIME, date_time);
        cv.put(DatabaseHelper.FLAG, flag);
        db.insert(DatabaseHelper.TABLE_MONTHLY_REPORT, null, cv);
        // TODO Auto-generated method stub

    }

    //update edited Monthly josn data
    public void updateMonthlyJsondata(int id,String json_data,
                               String date_time, String flag,String district,String vdc,String wardno,String startdate) {
        ContentValues cv = new ContentValues();

        cv.put(DatabaseHelper.JSON_DATA, json_data);
        cv.put(DatabaseHelper.DISTRICT, district);
        cv.put(DatabaseHelper.VDC, vdc);
        cv.put(DatabaseHelper.WARDNO, wardno);
        cv.put(DatabaseHelper.STARTDATE, startdate);
        cv.put(DatabaseHelper.DATE_TIME, date_time);
        cv.put(DatabaseHelper.FLAG, flag);
        db.update(DatabaseHelper.TABLE_EVENT_FORM_DATA, cv, "id=" + id, null);
        // TODO Auto-generated method stub

    }

    //update edited josn data
    public void updateJsondata(int id,String json_data,
                               String date_time, String flag,String district,String vdc,String wardno,String startdate) {
        ContentValues cv = new ContentValues();

        cv.put(DatabaseHelper.JSON_DATA, json_data);
        cv.put(DatabaseHelper.DISTRICT, district);
        cv.put(DatabaseHelper.VDC, vdc);
        cv.put(DatabaseHelper.WARDNO, wardno);
        cv.put(DatabaseHelper.STARTDATE, startdate);
        cv.put(DatabaseHelper.DATE_TIME, date_time);
        cv.put(DatabaseHelper.FLAG, flag);
        db.update(DatabaseHelper.TABLE_EVENT_FORM_DATA, cv, "id=" + id, null);
        // TODO Auto-generated method stub

    }



    //update only flag
    public void updateFlag(int id, String flag) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.FLAG, flag);
        db.update(DatabaseHelper.TABLE_EVENT_FORM_DATA, cv, "id=" + id, null);

    }


    //get all form list related to mobilizer
    public ArrayList<JsonData_DTO> getFormList(String username) {
        ArrayList<JsonData_DTO> getJsonDataList = new ArrayList<JsonData_DTO>();

        String sql1 = "Select * from tbl_event_form_data where flag=0 and username=\"" + username +"\" order by date_time DESC";

        Cursor cursor = db.rawQuery(sql1, null);

        while (cursor.moveToNext()) {

            System.out.println("cursorPosition:" + cursor.getPosition());
            System.out.println("cursorname:"
                    + cursor.getInt(cursor.getColumnIndex("id")));
            // info.setId(cursor.getInt(cursor.getColumnIndex("id")));

            int id = (cursor.getInt(cursor.getColumnIndex("id")));
            System.out.println("cursorname:" + id);

            String json_data = (cursor.getString(cursor
                    .getColumnIndex(DatabaseHelper.JSON_DATA)));

            String date_time = (cursor.getString(cursor
                    .getColumnIndex(DatabaseHelper.DATE_TIME)));
            String flag = (cursor.getString(cursor
                    .getColumnIndex(DatabaseHelper.FLAG)));

            getJsonDataList.add(new JsonData_DTO(id,json_data, date_time, flag,false));

        }
        cursor.close();
        return getJsonDataList;
    }



    //get all form Monthly list related to mobilizer
    public ArrayList<JsonData_DTO> getMonthlyFormList(String username) {
        ArrayList<JsonData_DTO> getJsonDataList = new ArrayList<JsonData_DTO>();

        String sql1 = "Select * from tbl_event_form_data where flag=0 and username=\"" + username +"\" order by date_time DESC";

        Cursor cursor = db.rawQuery(sql1, null);

        while (cursor.moveToNext()) {

            System.out.println("cursorPosition:" + cursor.getPosition());
            System.out.println("cursorname:"
                    + cursor.getInt(cursor.getColumnIndex("id")));
            // info.setId(cursor.getInt(cursor.getColumnIndex("id")));

            int id = (cursor.getInt(cursor.getColumnIndex("id")));
            System.out.println("cursorname:" + id);

            String json_data = (cursor.getString(cursor
                    .getColumnIndex(DatabaseHelper.JSON_DATA)));

            String date_time = (cursor.getString(cursor
                    .getColumnIndex(DatabaseHelper.DATE_TIME)));
            String flag = (cursor.getString(cursor
                    .getColumnIndex(DatabaseHelper.FLAG)));

            getJsonDataList.add(new JsonData_DTO(id,json_data, date_time, flag,false));

        }
        cursor.close();
        return getJsonDataList;
    }


  //for monthly report
  public ArrayList<JsonData_DTO> getMonthlyFormList(String username,String districtid,String vdc,String wardno,String month, String year) {

      ArrayList<JsonData_DTO> getJsonDataList = new ArrayList<JsonData_DTO>();

      String sql1 = "Select * from tbl_event_form_data where (username=\"" + username +"\" and district=\"" + districtid +"\" and vdc=\"" + vdc +"\" )or wardno=\"" + wardno +"\"  order by date_time DESC";

      Cursor cursor = db.rawQuery(sql1, null);

      while (cursor.moveToNext()) {

          System.out.println("cursorPosition:" + cursor.getPosition());
          System.out.println("cursorname:"
                  + cursor.getInt(cursor.getColumnIndex("id")));
          // info.setId(cursor.getInt(cursor.getColumnIndex("id")));

          int id = (cursor.getInt(cursor.getColumnIndex("id")));
          System.out.println("cursorname:" + id);

          String json_data = (cursor.getString(cursor
                  .getColumnIndex(DatabaseHelper.JSON_DATA)));

          String date_time = (cursor.getString(cursor
                  .getColumnIndex(DatabaseHelper.DATE_TIME)));
          String flag = (cursor.getString(cursor
                  .getColumnIndex(DatabaseHelper.FLAG)));

          getJsonDataList.add(new JsonData_DTO(id,json_data, date_time, flag,false));

      }
      cursor.close();
      return getJsonDataList;
  }


    //get sent list
    public ArrayList<JsonData_DTO> getSendFormList(String username) {
        ArrayList<JsonData_DTO> getJsonDataList = new ArrayList<JsonData_DTO>();
        String sql1 = "Select * from tbl_event_form_data where flag = 1  and username=\"" + username +"\"order by date_time";

        Cursor cursor = db.rawQuery(sql1, null);

        while (cursor.moveToNext()) {

            System.out.println("cursorPosition:" + cursor.getPosition());
            System.out.println("cursorname:"
                    + cursor.getInt(cursor.getColumnIndex("id")));
            // info.setId(cursor.getInt(cursor.getColumnIndex("id")));

            int id = (cursor.getInt(cursor.getColumnIndex("id")));
            System.out.println("cursorname:" + id);


            String json_data = (cursor.getString(cursor
                    .getColumnIndex(DatabaseHelper.JSON_DATA)));

            String date_time = (cursor.getString(cursor
                    .getColumnIndex(DatabaseHelper.DATE_TIME)));
            String flag = (cursor.getString(cursor
                    .getColumnIndex(DatabaseHelper.FLAG)));

            getJsonDataList.add(new JsonData_DTO(id,json_data, date_time, flag,false));

        }
        cursor.close();
        return getJsonDataList;
    }



//	public String getReviewData(int id) {
//
//
//		String edit_json_data = "";
//		String sql1 = "Select edit_json_data from tbl_form_data where id="+id;
//
//		Cursor cursor = db.rawQuery(sql1, null);
//
//		while (cursor.moveToNext()) {
//
//			edit_json_data = (cursor.getString(cursor
//					.getColumnIndex(DatabaseHelper.EDIT_JSON_DATA)));
//
//		}
//		cursor.close();
//		return edit_json_data;
//	}

    //local
    public String getJsonData(int id, String flag ) {


        String json_data = "";
        String sql1 = "Select json_data from tbl_event_form_data where id="+id ;

        Cursor cursor = db.rawQuery(sql1, null);

        while (cursor.moveToNext()) {

            json_data = (cursor.getString(cursor
                    .getColumnIndex(DatabaseHelper.JSON_DATA)));

        }
        cursor.close();
        return json_data;
    }





    //.....................////////////////


  	public void insertdistrict(String line){


            String[] separated = line.split(",");
           String districtid= separated[0]; // this will contain "Fruit"
            String districtname= separated[1];

            ContentValues cv = new ContentValues();
            cv.put(DatabaseHelper.DISTRICT_ID, districtid);
            cv.put(DatabaseHelper.DISTRICT_NAME, districtname);
            db.insert(DatabaseHelper.DISTRICT_INFO, null, cv);

      //  db.execSQL(sb.toString());

    }


        public ArrayList<DistrictInfo> getalldistrict() {
                ArrayList<DistrictInfo> userlist = new ArrayList<DistrictInfo>();
                String sql = "SELECT DistrictID,DistrictName FROM " + DatabaseHelper.DISTRICT_INFO;
                //System.out.println("sql==="+sql);

                Cursor cursor = db.rawQuery(sql, null);
                DistrictInfo info;
                try {
                        while (cursor.moveToNext()) {
                                String id = cursor.getString(cursor
                                        .getColumnIndex(DatabaseHelper.DISTRICT_ID));
                                String districtname = cursor.getString(cursor
                                        .getColumnIndex(DatabaseHelper.DISTRICT_NAME));

                                info = new DistrictInfo();
                                info.setId(id);
                                info.setDistrictname(districtname);
                                userlist.add(info);

                        }

                        return userlist;
                } finally {
                        cursor.close();
                }

        }
        public int getdistrictcount() {
                String countQuery = "SELECT  id FROM " +  DatabaseHelper.DISTRICT_INFO;

                Cursor cursor = db.rawQuery(countQuery, null);
                int cnt = cursor.getCount();
                cursor.close();
                return cnt;
        }


    public void insertvdc(String line){


        String[] separated = line.split(",");
        String vdcid= separated[0]; // this will contain "Fruit"
        String vcdname= separated[1];
        String districtid= separated[2];

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.VDC_ID, vdcid);
        cv.put(DatabaseHelper.DISTRICT_ID, districtid);
        cv.put(DatabaseHelper.VDC_NAME, vcdname);
        db.insert(DatabaseHelper.VDC_INFO, null, cv);

        //  db.execSQL(sb.toString());

    }
    public int getvdccount() {
        String countQuery = "SELECT  id FROM " +  DatabaseHelper.VDC_INFO;

        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }



    public ArrayList<VDCInfo> getvdc(String id ) {
        ArrayList<VDCInfo> vdclist = new ArrayList<VDCInfo>();
        String sql = "SELECT * FROM " + DatabaseHelper.VDC_INFO
                + " where DistrictID =" + id;
        //System.out.println("sql==="+sql);

        Cursor cursor = db.rawQuery(sql, null);
        VDCInfo info;
        try {
            while (cursor.moveToNext()) {
                String districtid = cursor.getString(cursor
                        .getColumnIndex(DatabaseHelper.DISTRICT_ID));
                String vdcname = cursor.getString(cursor
                        .getColumnIndex(DatabaseHelper.VDC_NAME));
                String vdcid = cursor.getString(cursor
                        .getColumnIndex(DatabaseHelper.VDC_ID));

                info = new VDCInfo();
                info.setDistrictid(districtid);
                info.setVdcname(vdcname);
                info.setVdcid(vdcid);
                vdclist.add(info);

            }

            return vdclist;
        } finally {
            cursor.close();
        }

    }

//insert sector

    public void insertsector(String line){


        String[] separated = line.split(",");
        String sectorid= separated[0]; // this will contain "Fruit"
        String sectorname= separated[1];


        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.SECTOR_ID, sectorid);
        cv.put(DatabaseHelper.SECTOR_NAME, sectorname);

        db.insert(DatabaseHelper.SECTOR_INFO, null, cv);

        //  db.execSQL(sb.toString());

    }

    //getarraylist of  sector
    public ArrayList<SectorInfo> getallsector() {
        ArrayList<SectorInfo> userlist = new ArrayList<SectorInfo>();
        String sql = "SELECT * FROM " + DatabaseHelper.SECTOR_INFO;
        //System.out.println("sql==="+sql);

        Cursor cursor = db.rawQuery(sql, null);
        SectorInfo info;
        try {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor
                        .getColumnIndex(DatabaseHelper.SECTOR_ID));
                String sectorname = cursor.getString(cursor
                        .getColumnIndex(DatabaseHelper.SECTOR_NAME));

                info = new SectorInfo();
                info.setSectorid(id);
                info.setSectorname(sectorname);
                userlist.add(info);

            }

            return userlist;
        } finally {
            cursor.close();
        }

    }


    public int getsectorcount() {
        String countQuery = "SELECT  id FROM " +  DatabaseHelper.SECTOR_INFO;

        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

//insert activity type
    public void insertactivitytype(String line){

     try {
         String[] separated = line.split(",");
         String activityid = separated[0]; // this will contain "Fruit"
         String acticvityname = separated[1];

         ContentValues cv = new ContentValues();
         cv.put(DatabaseHelper.PROGRAM_ID, activityid);
         cv.put(DatabaseHelper.PROGRAM_NAME, acticvityname);

         db.insert(DatabaseHelper.ACTIVITY_TYPE_INFO, null, cv);
     }catch (Exception e){
         System.out.println(e.getMessage());
     }

        //  db.execSQL(sb.toString());

    }

    //getarraylist of  activity
    public ArrayList<AcitivityTypeInfo> getallactivitytype() {
        ArrayList<AcitivityTypeInfo> userlist = new ArrayList<AcitivityTypeInfo>();
        String sql = "SELECT * FROM " + DatabaseHelper.ACTIVITY_TYPE_INFO;
        //System.out.println("sql==="+sql);

        Cursor cursor = db.rawQuery(sql, null);
        AcitivityTypeInfo info;
        try {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor
                        .getColumnIndex(DatabaseHelper.PROGRAM_ID));
                String activitytype = cursor.getString(cursor
                        .getColumnIndex(DatabaseHelper.PROGRAM_NAME));
                AcitivityTypeInfo infoactivity = new AcitivityTypeInfo();
                infoactivity.setActivityid(id);
                infoactivity.setActivitytype(activitytype);
                userlist.add(infoactivity);

            }

            return userlist;
        } finally {
            cursor.close();
        }

    }

    public int getactivitytypecount() {
        String countQuery = "SELECT  id FROM " +  DatabaseHelper.ACTIVITY_TYPE_INFO;

        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }
//activity name



    public void insertactivityname(String line){


        String[] separated = line.split(",");
        String activityid= separated[0];
        String activityname= separated[1];
        String sectorid= separated[2];

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.ACTIVITY_ID, activityid);
        cv.put(DatabaseHelper.ACTIVITY_NAME, activityname);
        cv.put(DatabaseHelper.SECTOR_ID, sectorid);
        db.insert(DatabaseHelper.ACTIVITY_INFO, null, cv);

        //  db.execSQL(sb.toString());

    }
    public int getactivitynamecount() {
        String countQuery = "SELECT  id FROM " +  DatabaseHelper.ACTIVITY_INFO;
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }



    public ArrayList<ActivityNameInfo> getactivityname(String id ) {
        ArrayList<ActivityNameInfo> activitynamelist = new ArrayList<ActivityNameInfo>();
        String sql = "SELECT * FROM " + DatabaseHelper.ACTIVITY_INFO
              + " where SectorId =" + id;
        System.out.println("sql==="+sql);

        Cursor cursor = db.rawQuery(sql, null);
        ActivityNameInfo info;
        try {
            while (cursor.moveToNext()) {
                String activityid = cursor.getString(cursor
                        .getColumnIndex(DatabaseHelper.ACTIVITY_ID));

                String activityname = cursor.getString(cursor
                        .getColumnIndex(DatabaseHelper.ACTIVITY_NAME));
                String sectorid = cursor.getString(cursor
                        .getColumnIndex(DatabaseHelper.SECTOR_ID));
                //System.out.println("sectorid==="+sectorid);

                info = new ActivityNameInfo();
                info.setActivityid(activityid);
                info.setActivityname(activityname);
                info.setSectorid(sectorid);
                activitynamelist.add(info);

            }

            return activitynamelist;
        } finally {
            cursor.close();
        }

    }




}
