package com.redcrosssociteyapp;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.redcrosssociteyapp.Model.DistrictInfo;
import com.redcrosssociteyapp.Model.MonthInfo;

import java.util.ArrayList;
import java.util.List;

public class MonthlyReportActivity extends AppCompatActivity {

    //Edittext
    EditText edittextdistrict, edittextmobilizername, edittextwardno, edittextsubmitteddate, edittextmajorachievement,
            edittextproblemsface, edittextsolutionapplied, edittextpointimprove, edittextplannext;
//listview
    ListView listviewactivitiescompleted;
//spinner
    Spinner spinnervdc,spinnermonth,spinneryear;
//monthly list adapter
    List<MonthInfo> monthList;
    //string
    String monthString,mtnString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.monthlyreport_layout);

        //edittext find
        // district
        edittextdistrict = (EditText) findViewById(R.id.edittextdistrict);
        //mobilizername
        edittextmobilizername = (EditText) findViewById(R.id.edittextmobilizername);
        //ward no
        edittextwardno = (EditText) findViewById(R.id.edittextwardno);
        //submitted date
        edittextsubmitteddate = (EditText) findViewById(R.id.edittextsubmitteddate);
        //major achievement
        edittextmajorachievement = (EditText) findViewById(R.id.edittextmajorachievement);
        //problem faced
        edittextproblemsface = (EditText) findViewById(R.id.edittextproblemsface);
        //solution applied
        edittextsolutionapplied = (EditText) findViewById(R.id.edittextsolutionapplied);
        //point to improved
        edittextpointimprove = (EditText) findViewById(R.id.edittextpointimprove);
        // plan for next month
        edittextplannext = (EditText) findViewById(R.id.edittextplannext);
        //activities  completed
        listviewactivitiescompleted = (ListView) findViewById(R.id.listviewactivitiescompleted);
        //vdc
        spinnervdc=(Spinner)findViewById(R.id.spinnervdc);
        //month
        spinnermonth=(Spinner)findViewById(R.id.spinnermonth);
        //year
        spinneryear=(Spinner)findViewById(R.id.spinneryear);

        //month list adapter call
        monthadpater();


    }

    //month list adpter

    public void monthadpater() {
        getmonth();

        ArrayAdapter<MonthInfo> monthadapter = new ArrayAdapter<MonthInfo>(
                getApplicationContext(), R.layout.spinner_list_item,
                monthList);
;
        spinnermonth.setAdapter(monthadapter);
        spinnermonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub


                monthString = String.valueOf(spinnermonth
                        .getSelectedItem());
                mtnString = monthList.get(arg2).getMth();


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }


public void getmonth(){
    monthList=new ArrayList<MonthInfo>();
    monthList.add(new MonthInfo("Jan", "01"));
    monthList.add(new MonthInfo("Feb", "02"));
    monthList.add(new MonthInfo("Mar", "03"));
    monthList.add(new MonthInfo("Apr", "04"));
    monthList.add(new MonthInfo("May", "05"));
    monthList.add(new MonthInfo("Jun", "06"));
    monthList.add(new MonthInfo("Jul", "07"));
    monthList.add(new MonthInfo("Aug", "08"));
    monthList.add(new MonthInfo("Sep", "09"));
    monthList.add(new MonthInfo("Oct", "10"));
    monthList.add(new MonthInfo("Nov", "11"));
    monthList.add(new MonthInfo("Dec", "12"));
}

}
