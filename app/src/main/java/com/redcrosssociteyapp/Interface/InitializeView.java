package com.redcrosssociteyapp.Interface;

import android.view.View;

/**
 * Created by Rohsik Mahrzn on 8/22/2016.
 */
public interface InitializeView {
    void InitializationView(View view);
}
