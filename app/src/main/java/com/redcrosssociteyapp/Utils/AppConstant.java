package com.redcrosssociteyapp.Utils;

/**
 * Created by Rohsik Mahrzn on 10/25/2016.
 */
public class AppConstant {



    //event report
    public static String SectorID= "SectorID";
    public static String SectorName= "SectorName";
    public static String SectorOthers= "SectorOthers";
    public static String ActivityId= "ActivityId";
    public static String ActivityName= "ActivityName";
    public static String ActivityOthers= "ActivityOthers";
    public static String ProgramTypeID= "ProgramTypeID";
    public static String ProgrammeTypeOthers= "ProgrammeTypeOthers";
    public static String DistrictId= "DistrictId";
    public static String DistrictName= "DistrictName";
    public static String VdcId= "VdcId";
    public static String WardNo= "WardNo";
    public static String OrganizationId= "OrganizationId";
    public static String StartDate= "StartDate";
    public static String EndDate= "EndDate";
    public static String ProgrammeArea= "ProgrammeArea";
    public static String ProgrammAtCommunity= "ProgrammAtCommunity";
    public static String ProgrammAtSchool= "ProgrammAtSchool";
    public static String ProgrammAtOtherPlace= "ProgrammAtOtherPlace";
    public static String TrainerName= "TrainerName";
    public static String DirectBenificiaryMale= "DirectBenificiaryMale";
    public static String DirectBenificiaryFemale= "DirectBenificiaryFemale";
    public static String DirectBenificiaryTotal= "DirectBenificiaryTotal";
    public static String DirectBenificiaryDalit= "DirectBenificiaryDalit";
    public static String DirectBenificiaryJanjati= "DirectBenificiaryJanjati";
    public static String DirectBenificiaryBrahmin= "DirectBenificiaryBrahmin";
    public static String DirectBenificiaryOtherCaste= "DirectBenificiaryOtherCaste";
    public static String DirectBenificiaryDisabledpeople= "DirectBenificiaryDisabledpeople";
    public static String DirectBenificiaryCoverage= "DirectBenificiaryCoverage";
    // jsonObject.put("DirectBenificiaryCoverage",);
    public static String IndirectBenificiaryMale= "IndirectBenificiaryMale";
    public static String IndirectBenificiaryFemale= "IndirectBenificiaryFemale";
    public static String IndirectBenificiaryTotal= "IndirectBenificiaryTotal";
    //  jsonObject.put("IndirectBenificiaryDisabledpeople","");
    public static String Remarks= "Remarks";
    public static String Presenter= "Presenter";
    public static String PresenterPostion= "PresenterPostion";
    public static String ReportSubmittedDate= "ReportSubmittedDate";
    public static String EnteredBy= "EnteredBy";
    public static String EnteredDate= "EnteredDate";
    public static String UpdatedBy= "UpdatedBy";
    public static String UpdatedDate= "UpdatedDate";



    //monthly report

    public static String DistrctId= "DistrctId";
   // public static String VdcId="VdcId";
   // public static String WardNo="WardNo";
    public static String MobilizerName="MobilizerName";
    public static String SubmittedDate="SubmittedDate";
    public static String Month="Month";
    public static String Year= "Year";
    public static String Acheivements="Acheivements";
    public static String ProblemsFaced="ProblemsFaced";
    public static String SolutionsApplied="SolutionsApplied";
    public static String PointsToImprove="PointsToImprove";
    public static String PlansForNextMonth="PlansForNextMonth";
    public static String DistrictCoordinatorApproved = "DistrictCoordinatorApproved";
    public static String DistrictCoordinateApprovedBy="DistrictCoordinateApprovedBy";
    public static String DistrictCoordinateApprovedDate="DistrictCoordinateApprovedDate";
    public static String CreatedBy="CreatedBy";
    public static String CreatedDate="CreatedDate";
    public static String District_DistrictId="District_DistrictId";

}
