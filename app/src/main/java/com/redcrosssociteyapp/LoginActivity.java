package com.redcrosssociteyapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.redcrosssociteyapp.DatabaseManager.DatabaseManager;
import com.redcrosssociteyapp.Utils.CheckNetwork;

public class LoginActivity extends AppCompatActivity {
    Button loginbutton;
    String username, password;
    DatabaseManager mDatabaseManager;
    String dbusername ,dbpassword;
    private EditText mPasswordView, mUsername;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setTitle("Red Cross Society");

//        setSupportActionBar(toolbar);

        mUsername= (EditText) findViewById(R.id.edittext_username);
        mPasswordView = (EditText) findViewById(R.id.edittext_password);

        loginbutton=(Button)findViewById(R.id.loginbutton);
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
//                startActivity(intent);
                if(isValidation()) {
                    if(username.equals("admin")&&password.equals("Admin123##")) {
                        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(LoginActivity.this,"Incorrect username or password",Toast.LENGTH_LONG).show();
                    }
                }

//                if (isValidation()) {
//                    if (mDatabaseManager.checkDatabase() != 0) {
//                        dbusername = mDatabaseManager.getUsername(username);
//                        dbpassword = mDatabaseManager.getPassword(password);
//
//                    } else {
//                        dbusername = "";
//                        dbpassword = "";
//                    }
//
//                    if (dbusername.equals(username) && dbpassword.equals(password)) {
//                        SharedPreferences sharedpreferences = getSharedPreferences("Login Credential", Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = sharedpreferences.edit();
//                        editor.putString("USERNAME", username);
//                        editor.putString("PASSWORD", password);
//                        editor.commit();
//                        startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
//                        finish();
//
//                    } else {
//
//                        if (CheckNetwork.checkInternetConnection(LoginActivity.this)) {
//                            attemptLogin(username, password);
//                        } else {
//                            Toast.makeText(LoginActivity.this, "Please Check your Network Connection.", Toast.LENGTH_SHORT);
//                        }
//                    }
//
//                }

            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void attemptLogin(final String username1, final String password1) {



        Ion.with(this).load("http://103.233.58.223/cwish/api/login")
                .setBodyParameter("UserName", username1)
                .setBodyParameter("Password", password1)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {

                                 @Override
                                 public void onCompleted(Exception e, JsonObject jsonObject) {
                                     // System.out.print("ResponseEEEEEE===" + arg0.toString());
                                     if (jsonObject != null) {


                                         System.out.print("Response===" + jsonObject.toString());

                                         Toast.makeText(LoginActivity.this, jsonObject.toString(), Toast.LENGTH_LONG).show();
                                         //  numberProgressBar.setVisibility(View.GONE);

                                         if (jsonObject.get("Status").getAsInt() == 200) {
                                             // mDatabaseManager.insertUserInfo(username,password,jsonObject.get("data").getAsString());
                                             mDatabaseManager.insertUserInfo(username, password, "");

                                             SharedPreferences sharedpreferences = getSharedPreferences("Login Credential", Context.MODE_PRIVATE);
                                             SharedPreferences.Editor editor = sharedpreferences.edit();
                                             editor.putString("USERNAME", username);
                                             editor.putString("PASSWORD", password);
                                             editor.commit();
                                             startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                                             finish();


                                         } else {
                                             new AlertDialog.Builder(LoginActivity.this)
                                                     .setTitle("Message from server")
                                                     .setMessage(jsonObject.toString())
                                                     .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                         public void onClick(DialogInterface dialog, int which) {
                                                             // continue with delete
                                                         }
                                                     })
                                                     .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                                         public void onClick(DialogInterface dialog, int which) {
                                                             // do nothing
                                                         }
                                                     })
                                                     .setIcon(android.R.drawable.ic_dialog_alert)
                                                     .show();
                                         }
                                     } else {
                                         Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                     }


                                 }
                             }
                );

    }

    public Boolean isValidation(){

        getEditTextValue();
        if (username.isEmpty()) {
            mUsername.setError(getString(R.string.error_field_required));
            return false;

        }
//        else if (!isEmailValid(username)) {
//            mEmailView.setError(getString(R.string.error_invalid_email));
//            return false;
//
        else if(password.isEmpty()){

            mPasswordView.setError(getString(R.string.error_field_required));
            return false;
        }

        return  true;
    }

    public void getEditTextValue(){
        username = mUsername.getText().toString();
        password = mPasswordView.getText().toString();
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

}
