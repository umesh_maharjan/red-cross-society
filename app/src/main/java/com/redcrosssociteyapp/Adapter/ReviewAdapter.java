package com.redcrosssociteyapp.Adapter;

/**
 * Created by Rohsik Mahrzn on 8/21/2016.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.redcrosssociteyapp.Fragment.EventListFragment;
import com.redcrosssociteyapp.Model.JsonData_DTO;
import com.redcrosssociteyapp.Model.ReviewItem;
import com.redcrosssociteyapp.R;
import com.redcrosssociteyapp.Utils.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public  class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.CustomViewHolder> {

    private Context mContext;

    private ArrayList<ReviewItem> reviewList = new ArrayList<ReviewItem>();
    private String eventName,districtName,startDate;
    public static CheckBox checkBox;
    private ArrayList<Integer> selections = new ArrayList<Integer>();

    public ReviewAdapter(Context c, ArrayList<ReviewItem> p) {
        this.mContext = c;
        this.reviewList=p;

    }



    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        View convertView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_review, null);
        return new CustomViewHolder(convertView);

    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {


      //  holder.llstatus.setVisibility(View.GONE);
        holder.textTitle.setText(reviewList.get(position).getTitle());
        holder.textValue.setText(reviewList.get(position).getDisplayValue());


    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView textTitle,textValue;

        public CustomViewHolder(View view) {
            super(view);
            this.textTitle = ((TextView) (view.findViewById(R.id.text1)));
            this.textValue = ((TextView) (view.findViewById(R.id.text2)));

        }
    }


}