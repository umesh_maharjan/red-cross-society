package com.redcrosssociteyapp.Adapter;

/**
 * Created by Rohsik Mahrzn on 8/21/2016.
 */

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.redcrosssociteyapp.Fragment.EventListFragment;
import com.redcrosssociteyapp.Model.JsonData_DTO;
import com.redcrosssociteyapp.R;
import com.redcrosssociteyapp.Utils.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public  class EventRecyclerAdapter extends RecyclerView.Adapter<EventRecyclerAdapter.CustomViewHolder> {

    private Context mContext;

    private ArrayList<JsonData_DTO> favList = new ArrayList<JsonData_DTO>();
    private String eventName,districtName,startDate;
    public static CheckBox checkBox;
    private ArrayList<Integer> selections = new ArrayList<Integer>();
    EventListFragment fragment;



    public EventRecyclerAdapter(Context c, ArrayList<JsonData_DTO> p, EventListFragment f) {
        this.mContext = c;
        this.favList=p;
        this.fragment=f;


    }



    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        View convertView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.default_fav_list_layout, null);

        return new CustomViewHolder(convertView);

    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {

        String request = favList.get(position).getJsonData();
        try {
            JSONObject object = new JSONObject(request);
            eventName = object.getString(AppConstant.ActivityName);
            districtName = object.getString(AppConstant.DistrictName);
            startDate = object.getString(AppConstant.StartDate);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

      //  holder.llstatus.setVisibility(View.GONE);
        holder.eventName.setText(eventName);
        holder.districtName.setText(districtName);
        holder.dateTime.setText(startDate);
        holder.checkbox.setTag(position);
        holder.checkbox.setChecked(favList.get(position).isSelected());

//        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//                if (isChecked) {
//
//                    selections.add(favList.get(position).getId());
//                    fragment.sendData(selections);
//
//                } else {
//
//                    selections.remove(favList.get(position).getId());
//                    fragment.sendData(selections);
//                    fragment.checkBox_all.setChecked(false);
//
//                }
//            }
//
//        }); // set the listener


        holder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (holder.checkbox.isChecked()) {

                    holder.checkbox.setChecked(true);
                    selections.add(favList.get(position).getId());
                    fragment.sendData(selections);


                } else {
                    holder.checkbox.setChecked(false);
                    selections.remove(favList.get(position).getId());
                     fragment.sendData(selections);
                    EventListFragment.checkBox_all.setChecked(false);

                }
            }

        });




//        fragment.checkBox_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//                //selections.clear();
//                if (isChecked) {
//
//                    for (int i = 0; i < favList.size(); i++) {
//                        //selections.add(favList.get(i).getId());
//                        favList.get(i).setSelected(isChecked);
//                    }
//
//                    fragment.sendData(selections);
//                    notifyDataSetChanged();
//
//                } else {
//                    //checkBox.setChecked(false);
//                    for (int i = 0; i < favList.size(); i++) {
//                        favList.get(i).setSelected(isChecked);
//                        selections.remove(favList.get(i).getId());
//                    }
//                    fragment.sendData(selections);
//                    notifyDataSetChanged();
//
//                }
//            }
//        }); // set the listener


        EventListFragment.checkBox_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                selections.clear();
                if (EventListFragment.checkBox_all.isChecked()) {

                    EventListFragment.checkBox_all.setChecked(true);
                    for (int i = 0; i < favList.size(); i++) {
                        selections.add(favList.get(i).getId());
                        favList.get(i).setSelected(true);

                    }
                    notifyDataSetChanged();
                    fragment.sendData(selections);


                } else {
                    //checkBox.setChecked(false);
                    for (int i = 0; i < favList.size(); i++) {
                        // favList.get(i).setSelected(isChecked);
                        selections.remove(favList.get(i).getId());
                    }
                    fragment.sendData(selections);
                    notifyDataSetChanged();

                }
            }

        });

    }

    @Override
    public int getItemCount() {
        return favList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView eventName,districtName;
        TextView dateTime;
        CheckBox checkbox;
       // LinearLayout llstatus;

        public CustomViewHolder(View view) {
            super(view);
            this.eventName = ((TextView) (view.findViewById(R.id.textViewEventName)));
            this.districtName = ((TextView) (view.findViewById(R.id.textViewDistrictName)));
            this.dateTime = ((TextView) (view.findViewById(R.id.textViewStartDate)));
            this.checkbox = (CheckBox) view.findViewById(R.id.checkBox_list);
           // this.llstatus=(LinearLayout) view.findViewById(R.id.ll_status);
        }
    }



}