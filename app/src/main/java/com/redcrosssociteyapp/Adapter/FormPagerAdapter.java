package com.redcrosssociteyapp.Adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.redcrosssociteyapp.Fragment.EventListFragment;


public class FormPagerAdapter extends FragmentStatePagerAdapter {


    public FormPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
      if(position ==0)
      {
          return new EventListFragment();
      }
        return new EventListFragment();
    }

    @Override
    public int getCount() {
        return 2;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        if(position==0)
        {
            return "Event List";
        }
            return "Sent Event List";

    }
}
