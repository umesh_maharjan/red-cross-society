package com.redcrosssociteyapp;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.redcrosssociteyapp.Adapter.FormPagerAdapter;

public class EventlistActivity extends AppCompatActivity {


    FormPagerAdapter formPagerAdapter;
    ViewPager viewPager;
    TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_form);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Event List");
       // getSupportActionBar().setTitle("Evemt List");
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        formPagerAdapter = new FormPagerAdapter(getSupportFragmentManager());
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager.setAdapter(formPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
