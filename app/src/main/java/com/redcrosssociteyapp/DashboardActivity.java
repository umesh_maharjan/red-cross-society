package com.redcrosssociteyapp;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;


public class DashboardActivity extends AppCompatActivity {
     LinearLayout addnewlinear,vieweventliner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dashboard_laypot);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Home");
        addnewlinear= (LinearLayout) findViewById(R.id.addnewlinear);
        vieweventliner= (LinearLayout) findViewById(R.id.vieweventliner);

        addnewlinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, FormActivity.class);

                startActivity(intent);
            }
        });

        vieweventliner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, EventlistActivity.class);

                startActivity(intent);
            }
        });


    }
}
