/**
� @author Kishor Maharjan
		  rohsik8@gmail.com
*/
package com.redcrosssociteyapp.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.koushikdutta.ion.builder.Builders;
import com.redcrosssociteyapp.Adapter.EventRecyclerAdapter;
import com.redcrosssociteyapp.Adapter.SentEventRecyclerAdapter;
import com.redcrosssociteyapp.DatabaseManager.DatabaseManager;
import com.redcrosssociteyapp.EditMainActivity;
import com.redcrosssociteyapp.Interface.InitializeView;
import com.redcrosssociteyapp.Model.JsonData_DTO;
import com.redcrosssociteyapp.R;
import com.redcrosssociteyapp.Utils.CheckNetwork;
import com.redcrosssociteyapp.Utils.ItemClickSupport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class SentEventListFragment extends Fragment implements InitializeView {

	RecyclerView listView;
	private LinearLayout contentLayout;
	DatabaseManager dbManager;
	TextView textVisible;
	private ArrayList<Integer> selections1= new ArrayList<Integer>();
	Menu menu;

	public static CheckBox checkBox_all;
	private static TextView select_all;
	private ArrayList<JsonData_DTO> favouriteList = new ArrayList<JsonData_DTO>();
	View view;
	public static final String MULTIPART_FORM_DATA = "multipart/form-data";
	public static final String MULTIPART_FORM_IMAGE = "image/*";
	ProgressDialog progress;
	NumberProgressBar numberProgressBar,dialognumberprocess;
	TextView countTextView;
	Button btn_logout;


	int count;
	int Totalsize;
	Dialog dialog;


	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		view = inflater.inflate(R.layout.default_fav_layout, null);
		numberProgressBar = (NumberProgressBar)view.findViewById(R.id.number_progress_bar);

		dbManager=new DatabaseManager(getActivity());
		InitializationView(view);
		//new GetFavouriteFromDatabase().execute();

		return  view;

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Add your menu entries here
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.send_main, menu);
		this.menu = menu;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		 if (id == R.id.action_send) {




			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void InitializationView(View view) {

		textVisible = (TextView)view.findViewById(R.id.textVisible);
		checkBox_all = (CheckBox)view.findViewById(R.id.checkBox_all);
		select_all =(TextView)view.findViewById(R.id.select_all);
		listView = (RecyclerView) view.findViewById(R.id.fav_list);
		listView.setHasFixedSize(true);
		listView.setItemAnimator(new DefaultItemAnimator());
		listView.setLayoutManager(new LinearLayoutManager(getActivity()));

	}

	private class GetFavouriteFromDatabase extends
			AsyncTask<String, Void, ArrayList<JsonData_DTO>> {

		@Override
		protected ArrayList<JsonData_DTO> doInBackground(String... params) {

			//get list from database
			favouriteList = dbManager.getFormList(getActivity().getSharedPreferences("Login Credential", Context.MODE_PRIVATE).getString("USERNAME",""));

			return favouriteList;
		}

		protected void onPostExecute(final ArrayList<JsonData_DTO> eventList) {

			if(eventList.size()==0){
				textVisible.setVisibility(View.VISIBLE);
			}


			//recycle view
			SentEventRecyclerAdapter adapter = new SentEventRecyclerAdapter(getActivity(), eventList, SentEventListFragment.this);
			listView.setAdapter(adapter);

			ItemClickSupport.addTo(listView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
				@Override
				public void onItemClicked(RecyclerView recyclerView, int position, View v) {
					// do it

					int listId = eventList.get(position).getId();
					Intent intent = new Intent(getActivity(), EditMainActivity.class);
					//intent.putExtra("LIST", "list");
					intent.putExtra("LIST_ID", listId);
					startActivity(intent);

				}
			});

		}
	}

		public void sendData(ArrayList<Integer> selections){

				this.selections1 = selections;

		}


	@Override
	public void onResume() {
		super.onResume();

		checkBox_all.setChecked(false);
		selections1.clear();
		new GetFavouriteFromDatabase().execute();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		//close the database
		//dbManager.close();

	}


	public void delete(File f) throws IOException {
		if (f.isDirectory()) {
			for (File c : f.listFiles()) {
				delete(c);
			}
		} else if (f.getAbsolutePath().endsWith("FIR")) {
			if (!f.delete()) {
				new FileNotFoundException("Failed to delete file: " + f);
			}
		}
	}

	public void postform(String data, final int id) {




		try {

			Builders.Any.B builder = Ion.with(getActivity()).load("http://103.233.58.223/cwish/api/requesthandler");
			builder.setLogging("UPLOAD LOGS:", Log.DEBUG)
					.uploadProgress(new ProgressCallback() {
						@Override
						public void onProgress(final long upload, final long total) {

							System.out.println("" + upload + " / " + total);
							getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									//stuff that updates ui
									dialognumberprocess.setVisibility(View.VISIBLE);
									dialognumberprocess.setMax((int) total);
									dialognumberprocess.setProgress((int) upload);
									dialognumberprocess.setClickable(false);
								}
							});

						}
					});

			//builder.setMultipartParameter("identifier", "Form");
			builder.setMultipartParameter("data", data);
			builder.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {

									 @Override
									 public void onCompleted(Exception arg0,
															 JsonObject jsonObject) {
										 if (jsonObject != null) {

											 count++;
											 if (jsonObject.get("Status").getAsInt() == 200) {

												 //numberProgressBar.setVisibility(View.GONE);
												// dialog.dismiss();
												//dbManager.updateFlag(id, "1");
												 if(Totalsize==count) {
													 dialog.dismiss();
													 new AlertDialog.Builder(getActivity())
															 .setTitle("Message from server")
															 .setMessage(jsonObject.get("Message").getAsString())
															 .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
																 public void onClick(DialogInterface dialog, int which) {

																	 new GetFavouriteFromDatabase().execute();

																 }
															 })

															 .setIcon(android.R.drawable.ic_dialog_alert)
															 .show();
													 count=0;
													 Totalsize=0;
												 }

											 } else {


												// numberProgressBar.setVisibility(View.GONE);
												 dialog.dismiss();
												 //dbManager.updateFlag(id,"1");

													new AlertDialog.Builder(getActivity())
															.setTitle("Message from server")
															.setMessage(jsonObject.get("Message").getAsString())
															.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
																public void onClick(DialogInterface dialog, int which) {
																	// continue with delete

																}
															})

															.setIcon(android.R.drawable.ic_dialog_alert)
															.show();

											 }

										 }else{


											 dialog.dismiss();
											 //dbManager.updateFlag(id,"1");

											 new AlertDialog.Builder(getActivity())
													 .setTitle("Message from server")
													 .setMessage("Server error Please try again")
													 .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
														 public void onClick(DialogInterface dialog, int which) {
															 // continue with delete

														 }
													 })

													 .setIcon(android.R.drawable.ic_dialog_alert)
													 .show();
										 }



									 }
								 }
					);


		}catch(Exception e){
			Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
		}
	}

//		public void showDialog(Activity activity,int totalCount,int pcount){
//			dialog = new Dialog(activity);
//			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//			dialog.setCancelable(false);
//			dialog.setContentView(R.layout.send_dialog_box);
//			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//			lp.copyFrom(dialog.getWindow().getAttributes());
//			lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//			lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//
//			countTextView = (TextView) dialog.findViewById(R.id.count_textView);
//			//countTextView.setText((pcount+1) + "/" + totalCount);
//			countTextView.setVisibility(View.GONE);
//			dialognumberprocess=(NumberProgressBar)dialog.findViewById(R.id.number_progress_bar);
//			dialog.show();
//			dialog.getWindow().setAttributes(lp);
//
//		}
//
//
//	public void customAlertDialogBox(Activity activity){
//		dialog = new Dialog(activity);
//		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		dialog.setCancelable(false);
//		dialog.setContentView(R.layout.acc_active_layout);
//		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//		lp.copyFrom(dialog.getWindow().getAttributes());
//		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//
//		btn_logout = (Button) dialog.findViewById(R.id.btn_logout);
//		//countTextView.setText((pcount+1) + "/" + totalCount);
//		btn_logout.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//
//				//dbManager.deleteUser(getActivity().getSharedPreferences("Login Credential", Context.MODE_PRIVATE).getString("USERNAME",""));
//				SharedPreferences sharedpreferences = getActivity().getSharedPreferences("Login Credential", Context.MODE_PRIVATE);
//				SharedPreferences.Editor editor = sharedpreferences.edit();
//				editor.putString("USERNAME", "");
//				editor.putString("PASSWORD", "");
//				editor.commit();
//				Intent intent = new Intent(getActivity(), LoginActivity.class);
//				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
//						Intent.FLAG_ACTIVITY_CLEAR_TASK);
//				startActivity(intent);
//				getActivity().finish();
//
//			}
//		});
//		dialog.show();
//		dialog.getWindow().setAttributes(lp);
//
//	}

}