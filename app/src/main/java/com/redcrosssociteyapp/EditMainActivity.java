package com.redcrosssociteyapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.redcrosssociteyapp.DatabaseManager.DatabaseManager;
import com.redcrosssociteyapp.Model.AcitivityTypeInfo;
import com.redcrosssociteyapp.Model.ActivityNameInfo;
import com.redcrosssociteyapp.Model.DistrictInfo;
import com.redcrosssociteyapp.Model.ReviewItem;
import com.redcrosssociteyapp.Model.SectorInfo;
import com.redcrosssociteyapp.Model.VDCInfo;
import com.redcrosssociteyapp.Utils.AppConstant;
import com.redcrosssociteyapp.Utils.MultiSpinner;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;


public class EditMainActivity extends AppCompatActivity {
    Spinner activityorganizedspinner, programvenue, sectorspinner, vdcspinner, activitytypespinner,programtypespinner;
    MultiSpinner partnersspinner;
    String districtString, venueString, sectorString, activitytypestring, programTypeOther;
    DatabaseManager db;
    EditText edittextenddate, edittextstartdate, edittextmale, edittextfemale, edittexttoatal,
            edittextindirectmale, edittextindirectfemale, edittextindirecttotal, edittextwardno, edittexttrainer,
            edittextdalit, edittextjantati, edittextbrahmin, edittextotherCaste, edittexthandicapped, edittextcomment,edittextcoverage,
    edittextacitivtytype;
    TextView textviewvenue;
    LinearLayout linearvenue, linearsector, linearactivity,linearacitivitype;
    private Calendar _calendar;
    int day, month, year;
    String daysString, monthsString, stringmale, stringfemale, directtotal,indirectMaleString, indirectFemaleString, districtidString,
            vdcnameString, vdcidString, wardnoString, venuenameString, sectorotherString,
            partnersString,
            activitytypeString, trainerString,dalitString,janajatiString,otherCasteString,handicappedString,brahminString,coveragestring,
    commentString,programType,sectoridString,StartDate,EndDate,activityIdString, programTypeId,vdcPosition,activityPosition;
    int totalmale = 0, totalfemale = 0, totalmalefemale;
    int maletotal, femaletotal, malefemaletotal;
    ArrayList<DistrictInfo> districtlist;
    ArrayList<VDCInfo> vdclist;
    ArrayList<SectorInfo> sectorlist;
    ArrayList<ActivityNameInfo> activitynamelist;
    ArrayList<AcitivityTypeInfo> programTypelist;
    Button btnsave;

    JSONObject obj;
    String jsonString;
    EditText editvenue, editsectorother, editactivitytpe;
    private ArrayAdapter<String> adapter;
    boolean[] selectedItems;

DatabaseManager dbManager;
    ArrayList<ReviewItem> reviewItemsDB = new ArrayList<ReviewItem>();
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_layout);

        sharedpreferences = getSharedPreferences("Login Credential", Context.MODE_PRIVATE);

        dbManager = new DatabaseManager(EditMainActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Edit Event Reporting Format");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        db = new DatabaseManager(this);

        jsonString = dbManager.getJsonData(getIntent().getIntExtra("LIST_ID", 0), "");

        activityorganizedspinner = (Spinner) findViewById(R.id.activityorganizedspinner);
        programvenue = (Spinner) findViewById(R.id.programvenue);
        sectorspinner = (Spinner) findViewById(R.id.sectorspinner);
        vdcspinner = (Spinner) findViewById(R.id.vdcspinner);
        // Button bt = (Button) findViewById(R.id.getSelected);
        partnersspinner = (MultiSpinner) findViewById(R.id.partnersspinner);
        activitytypespinner = (Spinner) findViewById(R.id.activitytypespinner);
        programtypespinner= (Spinner) findViewById(R.id.programtypespinner);
        linearvenue = (LinearLayout) findViewById(R.id.linearvenue);
        linearactivity = (LinearLayout) findViewById(R.id.linearactivity);
        linearsector = (LinearLayout) findViewById(R.id.linearsector);
        linearacitivitype = (LinearLayout) findViewById(R.id.linearacitivitype);
        edittextenddate = (EditText) findViewById(R.id.edittextenddate);
        edittextstartdate = (EditText) findViewById(R.id.edittextstartdate);
        edittextmale = (EditText) findViewById(R.id.edittextmale);
        edittextfemale = (EditText) findViewById(R.id.edittextfemale);
        edittexttoatal = (EditText) findViewById(R.id.edittexttoatal);
        edittextindirectmale = (EditText) findViewById(R.id.edittextindirectmale);
        edittextindirectfemale = (EditText) findViewById(R.id.edittextindirectfemale);
        edittextindirecttotal = (EditText) findViewById(R.id.edittextindirecttotal);
        edittexttrainer = (EditText) findViewById(R.id.edittexttrainer);
        edittextwardno = (EditText) findViewById(R.id.edittextwardno);
        edittextdalit = (EditText) findViewById(R.id.edittextdalit);
        edittextjantati = (EditText) findViewById(R.id.edittextjantati);
        edittextbrahmin = (EditText) findViewById(R.id.edittextbrahmin);
        edittextotherCaste = (EditText) findViewById(R.id.edittextothercaste);
        edittexthandicapped = (EditText) findViewById(R.id.edittexthandicapped);
        edittextcoverage = (EditText) findViewById(R.id.edittextcoverage);
        edittextcomment = (EditText) findViewById(R.id.edittextcomment);


        try {

             obj =new JSONObject(jsonString);
            // obj.getJSONObject(AppConstant.CaseType).put(AppConstant.CaseType, "");
            edittextenddate.setText(obj.getString(AppConstant.EndDate));
            edittextstartdate.setText(obj.getString(AppConstant.StartDate));
            edittextmale.setText(obj.getString(AppConstant.DirectBenificiaryMale));
            edittextfemale .setText(obj.getString(AppConstant.DirectBenificiaryFemale));
            edittexttoatal  .setText(obj.getString(AppConstant.DirectBenificiaryTotal));
            edittextindirectmale  .setText(obj.getString(AppConstant.IndirectBenificiaryMale));
            edittextindirectfemale  .setText(obj.getString(AppConstant.IndirectBenificiaryFemale));
            edittextindirecttotal .setText(obj.getString(AppConstant.IndirectBenificiaryTotal));
            edittexttrainer .setText(obj.getString(AppConstant.TrainerName));
            edittextwardno  .setText(obj.getString(AppConstant.WardNo));
            edittextdalit  .setText(obj.getString(AppConstant.DirectBenificiaryDalit));
            edittextjantati .setText(obj.getString(AppConstant.DirectBenificiaryJanjati));
            edittextbrahmin .setText(obj.getString(AppConstant.DirectBenificiaryBrahmin));
            edittextotherCaste .setText(obj.getString(AppConstant.DirectBenificiaryOtherCaste));
            edittextcoverage .setText(obj.getString(AppConstant.DirectBenificiaryCoverage));
            edittexthandicapped.setText(obj.getString(AppConstant.DirectBenificiaryDisabledpeople));
            edittextcomment.setText(obj.getString(AppConstant.Remarks));

        } catch (Exception e) {

        }


        btnsave = (Button) findViewById(R.id.btnsave);
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isvalidation()) {

                    JSONObject jsonObject = null;

                    try {
                        jsonObject = new JSONObject();
                        jsonObject.put("SectorID", sectoridString);
                        jsonObject.put("SectorName", sectorString);
                        jsonObject.put("SectorOthers", sectorotherString);
                        jsonObject.put("ActivityId", activityIdString);
                        jsonObject.put("ActivityName", activitytypeString);
                        jsonObject.put("ActivityPosition", activityPosition);
                        jsonObject.put("ActivityOthers", programTypeOther);
                        jsonObject.put("ProgramTypeID", programTypeId);
                        jsonObject.put("ProgramTypeName", programType);
                        jsonObject.put("ProgrammeTypeOthers", programTypeOther);
                        jsonObject.put("DistrictId", districtidString);
                        jsonObject.put("DistrictName", districtString);
                        jsonObject.put("VdcId", vdcidString);
                        jsonObject.put("VdcPosition", vdcPosition);
                        jsonObject.put("WardNo", wardnoString);
                        jsonObject.put("OrganizationId", partnersString);
                        jsonObject.put("StartDate", StartDate);
                        jsonObject.put("EndDate", EndDate);
                        jsonObject.put("ProgrammeArea", venueString);
                        jsonObject.put("ProgrammAtCommunity", venuenameString);
                        jsonObject.put("ProgrammAtSchool", venuenameString);
                        jsonObject.put("ProgrammAtOtherPlace", venuenameString);
                        jsonObject.put("TrainerName", trainerString);
                        jsonObject.put("DirectBenificiaryMale", stringmale);
                        jsonObject.put("DirectBenificiaryFemale", stringfemale);
                        jsonObject.put("DirectBenificiaryTotal", directtotal);
                        jsonObject.put("DirectBenificiaryDalit", dalitString);
                        jsonObject.put("DirectBenificiaryJanjati", janajatiString);
                        jsonObject.put("DirectBenificiaryBrahmin", brahminString);
                        jsonObject.put("DirectBenificiaryOtherCaste", otherCasteString);
                        jsonObject.put("DirectBenificiaryDisabledpeople", handicappedString);
                        jsonObject.put("DirectBenificiaryCoverage",coveragestring);
                        jsonObject.put("IndirectBenificiaryMale", indirectMaleString);
                        jsonObject.put("IndirectBenificiaryFemale", indirectFemaleString);
                        jsonObject.put("IndirectBenificiaryTotal", malefemaletotal);
                        //  jsonObject.put("IndirectBenificiaryDisabledpeople","");
                        jsonObject.put("Remarks", commentString);
                        jsonObject.put("Presenter", "");
                        jsonObject.put("PresenterPostion", "");
                        jsonObject.put("ReportSubmittedDate", "");
                        jsonObject.put("EnteredBy", "");
                        jsonObject.put("EnteredDate", "");
                        jsonObject.put("UpdatedBy", "");
                        jsonObject.put("UpdatedDate", "");
                    } catch (Exception e) {

                    }

                    CustomDialog(jsonObject);
//                    Toast.makeText(getApplicationContext(), "Data add successfully",
//                            Toast.LENGTH_LONG).show();
                }
            }
        });

        //fuction call gere ko
        getdate();
        partneradpater();
        venuadpater();
        districtadpater();
        sectooradapter();
        programTypeAdapter();

        edittextstartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(EditMainActivity.this,
                        startdatePickerListener, year, month, day);
                d.show();
            }
        });

        edittextenddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(EditMainActivity.this,
                        enddatePickerListener, year, month, day);
                d.show();
            }
        });

//direct male number listner
        edittextmale.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                getdirectparticipanttext();
                if (stringmale.equals("") && stringfemale.equals("")) {
                    totalmale = 0;
                    totalfemale = 0;
                    edittexttoatal.setText("0");

                } else if (stringmale.equals("") && !stringfemale.equals("")) {
                    totalmale = 0;
                    totalfemale = Integer.valueOf(stringfemale);
                    totalmalefemale = totalmale + totalfemale;
                    edittexttoatal.setText(totalmalefemale + "");
                } else if (!stringmale.equals("") && stringfemale.equals("")) {
                    totalmale = Integer.valueOf(stringmale);
                    totalfemale = 0;
                    totalmalefemale = totalmale + totalfemale;
                    edittexttoatal.setText(totalmalefemale + "");
                } else {
                    totalmale = Integer.valueOf(stringmale);
                    totalfemale = Integer.valueOf(stringfemale);
                    totalmalefemale = totalmale + totalfemale;
                    edittexttoatal.setText(totalmalefemale + "");

                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
//direct female number listner
        edittextfemale.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                getdirectparticipanttext();
                if (stringmale.equals("") && stringfemale.equals("")) {
                    totalmale = 0;
                    totalfemale = 0;
                    edittexttoatal.setText("0");

                } else if (stringmale.equals("") && !stringfemale.equals("")) {
                    totalmale = 0;
                    totalfemale = Integer.valueOf(stringfemale);
                    totalmalefemale = totalmale + totalfemale;
                    edittexttoatal.setText(totalmalefemale + "");
                } else if (!stringmale.equals("") && stringfemale.equals("")) {
                    totalmale = Integer.valueOf(stringmale);
                    totalfemale = 0;
                    totalmalefemale = totalmale + totalfemale;
                    edittexttoatal.setText(totalmalefemale + "");
                } else {
                    totalmale = Integer.valueOf(stringmale);
                    totalfemale = Integer.valueOf(stringfemale);
                    totalmalefemale = totalmale + totalfemale;
                    edittexttoatal.setText(totalmalefemale + "");

                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
        //indirect male number

        edittextindirectmale.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                getindirectparticipanttext();
                if (indirectMaleString.equals("") && indirectFemaleString.equals("")) {
                    maletotal = 0;
                    femaletotal = 0;
                    edittextindirecttotal.setText("0");

                } else if (indirectMaleString.equals("") && !indirectFemaleString.equals("")) {
                    maletotal = 0;
                    femaletotal = Integer.valueOf(indirectFemaleString);
                    malefemaletotal = maletotal + femaletotal;
                    edittextindirecttotal.setText(malefemaletotal + "");
                } else if (!indirectMaleString.equals("") && indirectFemaleString.equals("")) {
                    maletotal = Integer.valueOf(indirectMaleString);
                    femaletotal = 0;
                    malefemaletotal = maletotal + femaletotal;
                    edittextindirecttotal.setText(malefemaletotal + "");
                } else {
                    maletotal = Integer.valueOf(indirectMaleString);
                    femaletotal = Integer.valueOf(indirectFemaleString);
                    malefemaletotal = maletotal + femaletotal;
                    edittextindirecttotal.setText(malefemaletotal + "");

                }
            }


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });

        //indirectfe male number

        edittextindirectfemale.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub

                getindirectparticipanttext();
                if (indirectMaleString.equals("") && indirectFemaleString.equals("")) {

                    maletotal = 0;
                    femaletotal = 0;
                    edittextindirecttotal.setText("0");

                } else if (indirectMaleString.equals("") && !indirectFemaleString.equals("")) {
                    maletotal = 0;
                    femaletotal = Integer.valueOf(indirectFemaleString);
                    malefemaletotal = maletotal + femaletotal;
                    edittextindirecttotal.setText(malefemaletotal + "");
                } else if (!indirectMaleString.equals("") && indirectFemaleString.equals("")) {
                    maletotal = Integer.valueOf(indirectMaleString);
                    femaletotal = 0;
                    malefemaletotal = maletotal + femaletotal;
                    edittextindirecttotal.setText(malefemaletotal + "");
                } else {
                    maletotal = Integer.valueOf(indirectMaleString);
                    femaletotal = Integer.valueOf(indirectFemaleString);
                    malefemaletotal = maletotal + femaletotal;
                    edittextindirecttotal.setText(malefemaletotal + "");

                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });

    }


    //district spinner adapter
    public void districtadpater() {
        districtlist = new ArrayList<DistrictInfo>();
        districtlist = db.getalldistrict();
        ArrayAdapter<DistrictInfo> districtadapter = new ArrayAdapter<DistrictInfo>(
                getApplicationContext(), R.layout.spinner_list_item,
                districtlist);


//        ArrayAdapter<String> roomtypeadapter = new ArrayAdapter<String>(getApplicationContext(),
//                R.layout.spinner_list_item, districtlist);
        activityorganizedspinner.setAdapter(districtadapter);
        try {
            int spinnerPosition = Integer.valueOf(obj.getString(AppConstant.DistrictId));
            activityorganizedspinner.setSelection(spinnerPosition-1);
        }catch(Exception e){

        }


        activityorganizedspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub


                districtString = String.valueOf(activityorganizedspinner
                        .getSelectedItem());
                districtidString = districtlist.get(arg2).getId();

                if(districtidString!=null) {
                    vdcadpater();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    //vdc adapter
    public void vdcadpater() {
        vdclist = new ArrayList<VDCInfo>();
        vdclist = db.getvdc(districtidString);
        ArrayAdapter<VDCInfo> vdcadpter = new ArrayAdapter<VDCInfo>(
                getApplicationContext(), R.layout.spinner_list_item,
                vdclist);

//        ArrayAdapter<String> roomtypeadapter = new ArrayAdapter<String>(getApplicationContext(),
//                R.layout.spinner_list_item, districtlist);
        vdcspinner.setAdapter(vdcadpter);
        try {
            int spinnerPosition = Integer.valueOf(obj.getString("VdcPosition"));
           vdcspinner.setSelection(spinnerPosition);
        }catch(Exception e){

        }

        vdcspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                vdcnameString = String.valueOf(vdcspinner
                        .getSelectedItem());
                vdcidString = vdclist.get(arg2).getVdcid();
                vdcPosition = arg2+"";
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    //venue adapter
    public void venuadpater() {
        List<String> Venues = Arrays.asList(getResources().getStringArray(R.array.Venue));

        ArrayAdapter<String> venuetypeadapter = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_list_item, Venues);
        programvenue.setAdapter(venuetypeadapter);
       try {
           int spinnerPosition = venuetypeadapter.getPosition(obj.getString(AppConstant.ProgrammeArea).toString());
           programvenue.setSelection(spinnerPosition);
       }catch(Exception e){

        }

        programvenue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub


                venueString = String.valueOf(programvenue
                        .getSelectedItem());

                if (venueString.equals("Community")) {
                    linearvenue.removeAllViews();
                    editvenue = new EditText(getApplicationContext());
                    textview("Coomunity name", linearvenue);
                    editText(editvenue, linearvenue,venueString);
                } else if (venueString.equals("School")) {
                    linearvenue.removeAllViews();
                    editvenue = new EditText(getApplicationContext());

                    textview("School name", linearvenue);
                    editText(editvenue, linearvenue,venueString);
                } else {
                    linearvenue.removeAllViews();
                    editvenue = new EditText(getApplicationContext());

                    textview("Other venue Name", linearvenue);
                    editText(editvenue, linearvenue,venueString);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    //sector adapter
    public void sectooradapter() {

        sectorlist=new ArrayList<SectorInfo>();
        sectorlist=db.getallsector();
     //   List<String> Venues = Arrays.asList(getResources().getStringArray(R.array.Sector));

        ArrayAdapter<SectorInfo> sectortypeadapter = new ArrayAdapter<SectorInfo>(
                getApplicationContext(), R.layout.spinner_list_item,
                sectorlist);


        sectorspinner.setAdapter(sectortypeadapter);

        try {
            int spinnerPosition = Integer.valueOf(obj.getString(AppConstant.SectorID));
            sectorspinner.setSelection(spinnerPosition-1);
        }catch(Exception e){

        }
        sectorspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub


                sectorString = String.valueOf(sectorspinner
                        .getSelectedItem());

                sectoridString=sectorlist.get(arg2).getSectorid();

                System.out.println("sectoridString==="+sectoridString);
                if (sectorString.equals("Others")) {
                    linearsector.removeAllViews();
                    textview("Other sector", linearsector);

                    editsectorother = new EditText(getApplicationContext());
                    editText(editsectorother, linearsector,"sectorother");
                } else {
                    linearsector.removeAllViews();
                    activityadapter();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    //sector programTypeAdapter
    public void programTypeAdapter() {

        programTypelist =new ArrayList<AcitivityTypeInfo>();
        programTypelist =db.getallactivitytype();
        //   List<String> Venues = Arrays.asList(getResources().getStringArray(R.array.Sector));

        ArrayAdapter<AcitivityTypeInfo> activitytypespeadapter = new ArrayAdapter<AcitivityTypeInfo>(
                getApplicationContext(), R.layout.spinner_list_item,
                programTypelist);



        programtypespinner.setAdapter(activitytypespeadapter);

        try {
            int spinnerPosition = Integer.valueOf(obj.getString(AppConstant.ProgramTypeID));
            programtypespinner.setSelection(spinnerPosition-1);
        }catch(Exception e){

        }


        programtypespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                programType = String.valueOf(programtypespinner
                        .getSelectedItem());
                programTypeId =programTypelist.get(arg2).getActivityid();
                if (programType.equals("Others")) {
                    linearacitivitype.removeAllViews();
                    textview("Other program", linearacitivitype);

                    edittextacitivtytype = new EditText(getApplicationContext());
                    editText(edittextacitivtytype, linearacitivitype,"programother");
                } else {
                    linearacitivitype.removeAllViews();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    //acrtivity adapter

    public void activityadapter() {
       // List<String> activitytype = Arrays.asList(getResources().getStringArray(R.array.Activity));
        activitynamelist=new ArrayList<ActivityNameInfo>();

        activitynamelist=db.getactivityname(sectoridString);
        System.out.println("activitynamelist==" + activitynamelist);

        ArrayAdapter<ActivityNameInfo> activitynameadapter = new ArrayAdapter<ActivityNameInfo>(
                getApplicationContext(), R.layout.spinner_list_item,
                activitynamelist);



        activitytypespinner.setAdapter(activitynameadapter);
        try {
            int spinnerPosition = Integer.valueOf(obj.getString("ActivityPosition"));
            activitytypespinner.setSelection(spinnerPosition);
        }catch(Exception e){

        }

        activitytypespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub


                activitytypeString = String.valueOf(activitytypespinner
                        .getSelectedItem());
                activityIdString = activitynamelist.get(arg2).getActivityid();
                activityPosition = arg2+"";

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    //partners adapter

    public void partneradpater() {
        adapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item);
//        adapter.add("Select all");
        adapter.add("American Red Cross");
        adapter.add("Australian Red Cross");
        adapter.add("Belgium Red Cross ");
        adapter.add("British Red Cross");
        adapter.add("Canadian Red Cross");
        adapter.add("Danish Red Cross ");
        adapter.add("IFRC");
        adapter.add("Japanese Red Cross");
        adapter.add("Korean Red Cross");
        adapter.add("Nepal Red Cross");
        adapter.add("Norwegian Red Cross");
        adapter.add("Swiss Red Cross");
        adapter.add("Spanish Red Cross");
        adapter.add("Luxembourg Red Cross");

        partnersspinner.setAdapter(adapter, false, onSelectedListener);



//String items="Item3";
        try{
            selectedItems = new boolean[adapter.getCount()];
            String editString = obj.getString(AppConstant.OrganizationId);
           if(editString!=null) {
               partnersString = editString;
           }

            String[] separated = editString.split(",");

                for (int j = 0; j < selectedItems.length; j++) {

                    for(int i=0;i<separated.length;i++) {
                        if (separated[i].equals(adapter.getItem(j))) {
                            selectedItems[j] = true;
                        }
                    }

            }
            partnersspinner.setSelected(selectedItems);

        }catch(Exception e){

        }


//        selectedItems[0] = true;
//        // select first item
       //partnersspinner.setSelected(selectedItems);
    }

    private MultiSpinner.MultiSpinnerListener onSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            StringBuilder text = new StringBuilder();
            // Do something here with the selected items
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    text.append(adapter.getItem(i) + ",");

                }
            }

            String partners = text.substring(0, text.length() - 1);
            System.out.println("text===" + text);
            partnersString = partners;
            System.out.println("partners===" + partners);
        }
    };


//    public void partneradapter() {
//        List<String> Partner = Arrays.asList(getResources().getStringArray(R.array.Partners));
//
//        ArrayAdapter<String> partneradapter = new ArrayAdapter<String>(getApplicationContext(),
//                R.layout.spinner_list_item, Partner);
//        partnersspinner.setAdapter(partneradapter);
//        partnersspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> arg0, View arg1,
//                                       int arg2, long arg3) {
//                // TODO Auto-generated method stub
//
//
//                partnersString = String.valueOf(partnersspinner
//                        .getSelectedItem());
//
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//                // TODO Auto-generated method stub
//
//            }
//        });
//
//    }


    //edittext for community or school name
    private EditText editText(EditText editTextvenue, LinearLayout linearvenue,String type) {

        editTextvenue.setPadding(16, 16, 16, 16);

        editTextvenue.setTextColor(Color.parseColor("#000000"));
        editTextvenue.setBackgroundResource(android.R.drawable.editbox_background_normal);
        editTextvenue.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_CLASS_TEXT);
        linearvenue.addView(editTextvenue);

        try {
            if (type.equals("Community")) {
                editTextvenue.setText(obj.getString(AppConstant.ProgrammAtCommunity));
            } else if (type.equals("School")) {
                editTextvenue.setText(obj.getString(AppConstant.ProgrammAtSchool));
            } else if (type.equals("programother")) {
                editTextvenue.setText(obj.getString(AppConstant.ActivityOthers));
            }else if (type.equals("sectorother")) {
                editTextvenue.setText(obj.getString(AppConstant.SectorOthers));
            }
            else if(type.equals("Other")){
                editTextvenue.setText(obj.getString(AppConstant.ProgrammAtOtherPlace));
            }
        }catch (Exception e){

        }



        return editTextvenue;
    }

    //edittext for textview;
    private TextView textview(String name, LinearLayout linearvenue) {
        textviewvenue = new TextView(getApplicationContext());
        textviewvenue.setTextColor(Color.parseColor("#000000"));
        textviewvenue.setPadding(16, 16, 16, 16);
        textviewvenue.setTypeface(null, Typeface.BOLD);
        textviewvenue.setText(name);
        textviewvenue.setTextSize(14);

        linearvenue.addView(textviewvenue);
        return textviewvenue;
    }

    //get date
    public void getdate() {

        _calendar = Calendar.getInstance();
        day = _calendar.get(Calendar.DAY_OF_MONTH);

        if (day <= 9) {
            daysString = "0" + day + "";
        } else {
            daysString = day + "";
        }
        month = _calendar.get(Calendar.MONTH) + 1;

        if (month <= 9) {
            monthsString = "0" + month + "";
        } else {
            monthsString = month + "";
        }
        // completeRefresh();
        year = _calendar.get(Calendar.YEAR);

        edittextenddate.setText(year + "/" + monthsString + "/" + daysString);
        edittextstartdate.setText(year + "/" + monthsString + "/" + daysString);
    }
    //dialog box for  date

    private DatePickerDialog.OnDateSetListener startdatePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            int month = selectedMonth + 1;
            if (selectedDay <= 9) {
                daysString = "0" + selectedDay + "";
            } else {
                daysString = selectedDay + "";
            }


            if (month <= 9) {

                monthsString = "0" + month + "";
            } else {
                monthsString = month + "";
            }

            edittextstartdate.setText(selectedYear + "/" + monthsString + "/"
                    + daysString);
        }
    };

    private DatePickerDialog.OnDateSetListener enddatePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            int month = selectedMonth + 1;
            if (selectedDay <= 9) {
                daysString = "0" + selectedDay + "";
            } else {
                daysString = selectedDay + "";
            }


            if (month <= 9) {

                monthsString = "0" + month + "";
            } else {
                monthsString = month + "";
            }

            edittextenddate.setText(selectedYear + "/" + monthsString + "/"
                    + daysString);
        }
    };

    //get  direct participate number
    public void getdirectparticipanttext() {
        stringmale = edittextmale.getText().toString();
        stringfemale = edittextfemale.getText().toString();
    }

    //get indirect participant number
    public void getindirectparticipanttext() {
        indirectMaleString = edittextindirectmale.getText().toString();
        indirectFemaleString = edittextindirectfemale.getText().toString();
    }

    public void getedittextvalue() {

        getindirectparticipanttext();
        getdirectparticipanttext();
        wardnoString = edittextwardno.getText().toString();
        venuenameString = editvenue.getText().toString();
        trainerString = edittexttrainer.getText().toString();
        StartDate= edittextstartdate.getText().toString();
        EndDate= edittextenddate.getText().toString();
        activitytypeString= edittexttrainer.getText().toString();
                trainerString= edittexttrainer.getText().toString();
        dalitString= edittextdalit.getText().toString();
                janajatiString= edittextjantati.getText().toString();
        otherCasteString= edittextotherCaste.getText().toString();
                handicappedString= edittexthandicapped.getText().toString();
        directtotal= edittexttoatal.getText().toString();
        brahminString= edittextbrahmin.getText().toString();
                commentString= edittextcomment.getText().toString();
        coveragestring = edittextcoverage.getText().toString();


        if (sectorString.equals("Other")) {
            sectorotherString = editsectorother.getText().toString();
        }
        if (activitytypeString.equals("Other")) {
            programTypeOther = editactivitytpe.getText().toString();
        }

    }

    public boolean isvalidation() {
        getedittextvalue();
        if (wardnoString.equals("")) {
            edittextwardno.setError(getResources().getString(R.string.requiredfield));
            return false;
        } else if (venuenameString.equals("")) {
            editvenue.setError(getResources().getString(R.string.requiredfield));
            return false;
        } else if (trainerString.equals("")) {
            edittexttrainer.setError(getResources().getString(R.string.requiredfield));
            return false;
        } else if (sectorString.equals("Other") && sectorotherString.equals("")) {

            editsectorother.setError(getResources().getString(R.string.requiredfield));


            return false;
        } else if (activitytypeString.equals("Other") && programTypeOther.equals("")) {

            editactivitytpe.setError(getResources().getString(R.string.requiredfield));


            return false;
        } else
            return true;
    }


    protected void CustomDialog(final JSONObject jsonObject) {
        // TODO Auto-generated method stub

        try {


            Iterator keysToCopyIterator = jsonObject.keys();
            while(keysToCopyIterator.hasNext()) {
                String title = (String) keysToCopyIterator.next();
                String value = jsonObject.getString(title);

                reviewItemsDB.add(new ReviewItem(title, value));
                //System.out.println("");
            }


        }catch (Exception e){

            System.out.println(e.getMessage());
        }



        final  AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Confirmation");
        dialog.setMessage("Do you want to save edited form?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        // TODO Auto-generated method stubI

                        dbManager.updateJsondata(getIntent().getIntExtra("LIST_ID", 0),  jsonObject.toString(), String.valueOf(System.currentTimeMillis()), "0");
                        Toast.makeText(EditMainActivity.this, "Succesfully Edited", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });

        dialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        // TODO Auto-generated method stubI

                    }
                });


        dialog.show();

    }

}
