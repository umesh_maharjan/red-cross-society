package com.redcrosssociteyapp.Model;

/**
 * Created by govinda on 10/3/2016.
 */
public class DistrictInfo {
    private String id;
    private String districtname;
    private int position;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDistrictname() {
        return districtname;
    }

    public void setDistrictname(String districtname) {
        this.districtname = districtname;
    }

    @Override
    public String toString()
    {
        return this.districtname;

    }
}
