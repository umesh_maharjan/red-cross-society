package com.redcrosssociteyapp.Model;


public class SectorInfo {
    private String sectorid;
    private String sectorname;

    public String getSectorid() {
        return sectorid;
    }

    public void setSectorid(String sectorid) {
        this.sectorid = sectorid;
    }

    public String getSectorname() {
        return sectorname;
    }

    public void setSectorname(String sectorname) {
        this.sectorname = sectorname;
    }

    @Override
    public String toString()
    {
        return this.sectorname;

    }
}
