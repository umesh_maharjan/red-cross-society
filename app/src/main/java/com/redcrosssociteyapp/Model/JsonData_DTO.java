package com.redcrosssociteyapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rohsik Mahrzn on 7/24/2016.
 */
public class JsonData_DTO implements Serializable, Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("jsonData")
    private String jsonData;

    @SerializedName("data_time")
    private String data_time;

    @SerializedName("flag")
    private String flag;

    @SerializedName("selected")
    private boolean selected;

    public JsonData_DTO(int id,String jsonData, String data_time, String flag,boolean selected){

        this.id = id;

        this.jsonData = jsonData;

        this.data_time = data_time;
        this.flag = flag;
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getData_time() {
        return data_time;
    }

    public void setData_time(String data_time) {
        this.data_time = data_time;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }


    JsonData_DTO info;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(id);

        dest.writeString(jsonData);

        dest.writeString(data_time);
        dest.writeString(flag);
        dest.writeParcelable(info,flags);

    }

    public JsonData_DTO(Parcel in){
        id = in.readInt();

        jsonData = in.readString();

        data_time = in.readString();
        flag = in.readString();
        info = in.readParcelable(JsonData_DTO.class.getClassLoader());

    }

    public static final Creator<JsonData_DTO> CREATOR
            = new Creator<JsonData_DTO>() {

        // This simply calls our new constructor (typically private) and
        // passes along the unmarshalled `Parcel`, and then returns the new object!
        @Override
        public JsonData_DTO createFromParcel(Parcel in) {
            return new JsonData_DTO(in);
        }

        // We just need to copy this and change the type to match our class.
        @Override
        public JsonData_DTO[] newArray(int size) {
            return new JsonData_DTO[size];
        }
    };
}
