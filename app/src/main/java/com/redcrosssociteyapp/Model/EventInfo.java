package com.redcrosssociteyapp.Model;

/**
 * Created by govinda on 10/6/2016.
 */
public class EventInfo {
     private String eventname;

    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    public EventInfo(String eventname){
        this.eventname=eventname;
    }

}
