package com.redcrosssociteyapp.Model;

/**
 * Created by govinda on 10/3/2016.
 */
public class VDCInfo {
    private String districtid;
    private String vdcid;
    private String vdcname;

    public String getDistrictid() {
        return districtid;
    }

    public void setDistrictid(String districtid) {
        this.districtid = districtid;
    }

    public String getVdcid() {
        return vdcid;
    }

    public void setVdcid(String vdcid) {
        this.vdcid = vdcid;
    }

    public String getVdcname() {
        return vdcname;
    }

    public void setVdcname(String vdcname) {
        this.vdcname = vdcname;
    }
    @Override
    public String toString()
    {
        return this.vdcname;

    }
}
