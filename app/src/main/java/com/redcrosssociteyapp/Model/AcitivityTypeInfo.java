package com.redcrosssociteyapp.Model;

public class AcitivityTypeInfo {
    private String activityid;
    private String activitytype;

    public String getActivityid() {
        return activityid;
    }

    public void setActivityid(String activityid) {
        this.activityid = activityid;
    }

    public String getActivitytype() {
        return activitytype;
    }

    public void setActivitytype(String activitytype) {
        this.activitytype = activitytype;
    }
    @Override
    public String toString()
    {
        return this.activitytype;

    }
}
