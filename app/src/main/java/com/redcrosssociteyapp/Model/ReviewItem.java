package com.redcrosssociteyapp.Model;

/**
 * Created by Rohsik Mahrzn on 10/24/2016.
 */
public class ReviewItem {


    private String mTitle;
    private String mDisplayValue;


    public  ReviewItem(){

    }

    public ReviewItem(String title, String displayValue) {
        mTitle = title;
        mDisplayValue = displayValue;

    }

    public String getDisplayValue() {
        return mDisplayValue;
    }

    public void setDisplayValue(String displayValue) {
        mDisplayValue = displayValue;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }


}
