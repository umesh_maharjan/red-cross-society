package com.redcrosssociteyapp.Model;


public class ActivityNameInfo {

    private String activityid;
    private String activityname;
    private String sectorid;

    public String getActivityid() {
        return activityid;
    }

    public void setActivityid(String activityid) {
        this.activityid = activityid;
    }

    public String getActivityname() {
        return activityname;
    }

    public void setActivityname(String activityname) {
        this.activityname = activityname;
    }

    public String getSectorid() {
        return sectorid;
    }

    public void setSectorid(String sectorid) {
        this.sectorid = sectorid;
    }
    @Override
    public String toString()
    {
        return this.activityname;

    }

}
